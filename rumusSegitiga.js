// function getLuasSegitiga(a, t) {
//   return (a * t) / 2;
// }

function getKelilingSegitiga(sisiA, sisiB, sisiC) {
  return sisiA + sisiB + sisiC;
}

module.exports = {
  getKelilingSegitiga,
  getLuasSegitiga: (a, t) => {
    return (a * t) / 2;
  },
};

