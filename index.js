console.log("this is myy first file");
// core module ('os') atau __dirname
const os = require("os");

console.log("core modules", os.freemem());

// third party module is-even atau is-odd
const isOdd = require("is-odd");

console.log("apakah number ini ganjil", isOdd(4));

// local module function yang kita bikin

const rumusSegitiga = require("./rumusSegitiga");
const functionLain = require("./functionLain");
console.log(
  "keliling segitiganya berapa mase",
  rumusSegitiga.getKelilingSegitiga(1, 3, 5)
);

console.log(
  "luas segitiganya berapa mase",
  rumusSegitiga.getLuasSegitiga(1, 3)
);
