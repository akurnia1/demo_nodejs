const fs = require("fs");

function getListMovies() {
  const movies = fs.readFileSync("./movies.json", "utf-8");
  return JSON.parse(movies);
}

function getDetailMovie(title) {
  const movies = getListMovies();
  movies.forEach((movie) => {
    if (movie.title === title) {
      console.log("movie", movie);
    }
  });
}
function addMovie(movie) {
  const movies = getListMovies();
  movies.push(movie);
  console.log("movies", movies);
  fs.writeFileSync("./movies.json", JSON.stringify(movies));
}

function deleteMovie(title) {
  const movies = getListMovies();
  const moviesFiltered = movies.filter((movie) => {
    return movie.title !== title;
  });
  console.log("moviesFiltered", moviesFiltered);
  fs.writeFileSync("./movies.json", JSON.stringify(moviesFiltered));
}

console.log("process.argv", process.argv);

console.log("process.argv[2]", process.argv[2]);

if (Number(process.argv[2]) === 1) {
  console.log("masuk sini gak");
  const movies = getListMovies();
  console.log("movies", movies);
}

if (Number(process.argv[2]) == 2) {
  getDetailMovie(process.argv[3]);
}

if (Number(process.argv[2]) == 3) {
  const movie = {
    title: process.argv[3],
    genre: process.argv[4],
    duration: process.argv[5],
    studio: process.argv[6],
  };
  //   console.log("movie", movie);
  addMovie(movie);
}
